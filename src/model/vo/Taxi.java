package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	//-----------------------------------------------
	//	ATRIBUTOS
	//-----------------------------------------------

	private String taxiId;
	private	String company;


	//-----------------------------------------------
	//	COSTRUCTOR
	//-----------------------------------------------

	public Taxi(String pTaxiId, String pCompany){
		taxiId=pTaxiId;
		company=pCompany;
	}
	
	public Taxi(String pTaxiId){
		taxiId=pTaxiId;
		
	}
	
	//-----------------------------------------------
	//	METODOS
	//-----------------------------------------------

	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}

	@Override
	public int compareTo(Taxi o) {
		int resp = 0;
		if(taxiId.compareTo(o.getTaxiId())<0){
			resp=-1;
		}
		else if(taxiId.compareTo(o.getTaxiId())>0){
			resp=1;
		}
		else{
			resp=0;
		}
		return resp;
	}	
}
