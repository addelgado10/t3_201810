package model.vo;

import java.sql.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String tripId;
	private String taxiId;
	private int tripSec;
	private double tripMiles;
	private double tripTotal;
	private Taxi taxi;	
	private String comunityArea;
	private Date timeStartTimestamp;
	private Date timeEndTimestamp;

	public Service (String pTripId, String pTaxiId, String pTripSec, String pTripMiles, String pTripTotal, String pComunityArea){
		String tripId = pTripId;
		String taxiId = pTaxiId;
		tripSec = Integer.parseInt(pTripSec);
		tripMiles = Double.parseDouble(pTripMiles);
		tripTotal = Double.parseDouble(pTripTotal);
		Taxi taxi = new Taxi (pTaxiId);
		comunityArea=pComunityArea;
	}

	public Service (String pTripId, String pTaxiId, String pTripSec, String pTripMiles, String pTripTotal, String pCompany, String pComunityArea, Date pTimeStartTimestamp,Date pTimeEndtTimestamp){
		String tripId = pTripId;
		String taxiId = pTaxiId;
		tripSec = Integer.parseInt(pTripSec);
		tripMiles = Double.parseDouble(pTripMiles);
		tripTotal = Double.parseDouble(pTripTotal);
		Taxi taxi = new Taxi (pTaxiId,pCompany);
		comunityArea=pComunityArea;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return tripId;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxiId;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return tripSec;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return tripTotal;
	}
	
	public String getComunityArea(){
		return comunityArea;
	}
	
	public Date getTimeStart(){
		return timeStartTimestamp;
	}
	
	public Date getTimeEnd(){
		return timeEndTimestamp;
	}

	@Override
	public int compareTo(Service o) {
		int resp = 0;
		if(tripId.compareTo(o.getTripId())<0){
			resp=-1;
		}
		else if(tripId.compareTo(o.getTripId())>0){
			resp=1;
		}
		else{
			resp=0;
		}
		return resp;
	}

	public Taxi getTaxi (){
		return taxi;
	}

	public void setTaxi(String pTaxiId){
		taxi=new Taxi(pTaxiId);
	}

	public void setTaxi(String pTaxiId,String pCompany){
		taxi=new Taxi(pTaxiId,pCompany);
	}

}
