package model.data_structures;

public class Node<Comparable> {
	private Comparable item;
	private	Node next;
	
	public Node(Comparable item){
		this.item=item;
	}
	
	public Comparable getItem(){
		return item;
	}
	
	public Node next(){
		return next;
	}
	
	public void setNextNode(Node node){
		next=node;
	}

}
