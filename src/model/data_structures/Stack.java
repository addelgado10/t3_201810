package model.data_structures;

import java.util.Iterator;

public class Stack<Comparable>{

	private Node<Comparable> first;
	private int n;

	public Stack(Comparable item){
		first=new Node<Comparable>(item);
		n++;
	}
	
	public boolean isEmpty(){
		return first==null;
	}
	
	public int size(){
		return n;
	}
	
	public void push(Comparable pItem){
		Node add = new Node<Comparable>(pItem);
		add.setNextNode(first);
		first=add;
		n++;
	}
	public Comparable remove(){
		Comparable resp=(Comparable) first.getItem();
		first=first.next();
		n--;
		return resp;
	}


	
}