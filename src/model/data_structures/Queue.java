package model.data_structures;

import java.util.Iterator;

public class Queue<Comparable>  {

	private Node<Comparable> first;
	private Node<Comparable> last;
	private int n;

	public Queue(Comparable item){
		first=new Node<Comparable>(item);
		last = first;
		n++;
	}
	
	public boolean isEmpty(){
		return first==null;
	}
	
	public int size(){
		return n;
	}
	
	public void enqueue(Comparable item){
		Node add = new Node<Comparable>(item);
		last.setNextNode(add);
		if(isEmpty()){
			first=last;
		}
		else{
			last=add;
		}
		n++;
	}
	
	public Comparable dequeue(){
		Comparable resp = (Comparable) first.getItem();
		first=first.next();
		if(isEmpty()){
			last=null;
		}
		n--;
		return resp;
	}


}
