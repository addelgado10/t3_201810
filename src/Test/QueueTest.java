package Test;
import model.data_structures.Queue;
import junit.framework.TestCase;

public class QueueTest extends TestCase{

	private Queue queue;

	public void escenario1(){
		String prueba = "123";
		Comparable item = (Comparable)prueba;
		queue=new Queue<Comparable>(item);
	}

	public void escenario2(){

		String a="a";
		String b="b";
		String c="c";
		String d="d";

		Comparable itema = (Comparable)a;
		Comparable itemb = (Comparable)b;
		Comparable itemc = (Comparable)c;
		Comparable itemd = (Comparable)d;

		queue=new Queue<Comparable>(itema);
		queue.enqueue(itemb);
		queue.enqueue(itemc);
		queue.enqueue(itemd);
	}

	public void testIsEmptyYsize(){
		escenario1();

		String e= (String) queue.dequeue();
		
		assertEquals("La cola no est� vac�a", 0,queue.size());
		assertEquals("La lista no est� vac�a",true ,queue.isEmpty());
		assertEquals("La lista tiene mas de cero elementos", 0,queue.size());
	}

	public void testdequeue(){
		escenario2();
		
		String itemA = (String) queue.dequeue();
		String itemB = (String) queue.dequeue();
		String itemC = (String) queue.dequeue();
		String itemD = (String) queue.dequeue();
		
		assertEquals("La cola no devolvio el valor esperado", "a",itemA);
		assertEquals("La cola no devolvio el valor esperado", "b",itemB);
		assertEquals("La cola no devolvio el valor esperado", "c",itemC);
		assertEquals("La cola no devolvio el valor esperado", "d",itemD);
	}
	
	public void testEnqueue(){
		escenario1();
		
		int item1=1;
		int item2=2;
		
		queue.enqueue(item1);
		queue.enqueue(item2);
	
		String xXx=(String)queue.dequeue();
		
		assertEquals("Los valores no se agregaron a la cola", 1,queue.dequeue());
		assertEquals("Los valores no se agregaron a la cola", 2,queue.dequeue());
	}

}
